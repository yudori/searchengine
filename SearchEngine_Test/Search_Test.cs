﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SearchEngine;
using System;
using System.Collections.Generic;

namespace Search_Test
{
    [TestClass]
    public class Search_Test
    {

        private static string dir = @"C:\SearchEngine\test_files\";
        private Searcher s = new Searcher(dir);

        private Dictionary<String, List<String>> GetTestIndex()
        {
            Dictionary<String, List<String>> test = new Dictionary<string,List<string>>();
            List<Dictionary<String, List<String>>> test_allFiles = FileReader.readAll(dir);

            foreach (Dictionary<string, List<string>> dict in test_allFiles)
            {
                foreach (KeyValuePair<string, List<string>> k in dict)
                {
                    test.Add(k.Key, k.Value);
                }
            }

            return test;
        }

        [TestMethod]
        public void GetIndexTable_Test1()
        {
            string path = dir + "ccc.xls";
            Dictionary<String, List<String>> curSearch = FileReader.read(path);
            SortedDictionary<string, Term> tmpIndex = Search.GetIndexTable(curSearch);
            bool actual = tmpIndex["password"].Documents.Contains(tmpIndex["password"].Documents.Find(n => n.DocID == path));
            Assert.IsTrue(actual == true, "Error");
        }

        [TestMethod]
        public void GetIndexTable_Test2()
        {
            string path = dir + "322.docx";
            Dictionary<String, List<String>> curSearch = FileReader.read(path);
            SortedDictionary<string, Term> tmpIndex = Search.GetIndexTable(curSearch);
            bool actual = tmpIndex["design"].Documents.Contains(tmpIndex["design"].Documents.Find(n => n.DocID == path));
            Assert.IsTrue(actual == true, "Error");
        }

        [TestMethod]
        public void GetIndexTable_Test3()
        {
            string path = dir + "ttt.txt";
            Dictionary<String, List<String>> curSearch = FileReader.read(path);
            SortedDictionary<string, Term> tmpIndex = Search.GetIndexTable(curSearch);
            bool actual = tmpIndex["gmail"].Documents.Contains(tmpIndex["gmail"].Documents.Find(n => n.DocID == path));
            Assert.IsTrue(actual == true, "Error");
        }

        [TestMethod]
        public void Query_Test1()
        {
            List<string> res = Search.Query(Search.GetIndexTable(GetTestIndex()), "gmail");
            Assert.IsTrue(res.Count == 5);
        }

        [TestMethod]
        public void Query_Test2()
        {
            List<string> res = Search.Query(Search.GetIndexTable(GetTestIndex()), "almost");
            Assert.IsTrue(res.Count == 0);
        }

        [TestMethod]
        public void Query_Test3()
        {
            List<string> res = Search.Query(Search.GetIndexTable(GetTestIndex()), "lightning speed my friend");
            Assert.IsTrue(res.Count == 0);
        }

        [TestMethod]
        public void Query_Test4()
        {
            List<string> res = Search.Query(Search.GetIndexTable(GetTestIndex()), "gmail");
            Assert.IsTrue(res.Contains(@"C:\SearchEngine\test_files\ccc.xls".ToLower()));
        }

        [TestMethod]
        public void Query_Test5()
        {
            List<string> res = Search.Query(Search.GetIndexTable(GetTestIndex()), "halimat");
            Assert.IsTrue(res.Contains(@"C:\SearchEngine\test_files\ccc.xls".ToLower()));
        }

        [TestMethod]
        public void Query_Test7()
        {
            List<string> res = Search.Query(Search.GetIndexTable(GetTestIndex()), "design");
            Assert.IsTrue(res.Contains(@"C:\SearchEngine\test_files\co.pdf".ToLower()));
        }
    }
}
