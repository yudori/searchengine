﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SearchEngine;
using System.Collections.Generic;
using System.IO;

namespace SearchEngine_Test
{
    [TestClass]

    public class FileReader_Test
    {
        string dir = @"C:\SearchEngine\test_files\";

        [TestMethod]

        public void xmlTest1()
        {
            string path = dir + "MyXml.xml";
            Dictionary<string, List<string>> st = FileReader.read(path);
            string actual = st[path][0];
            string expected = "npoi";
            Assert.AreEqual(expected, actual, "Error!");
        }

        [TestMethod]
        public void xmlTest2()
        {
            string path = dir + "MyXml.xml";
            Dictionary<string, List<string>> st = FileReader.read(path);
            string actual = st[path][st[path].Count - 1];
            string expected = "class";
            Assert.AreEqual(expected, actual, "Error!");
        }

        [TestMethod]
        public void xlsTest1()
        {
            string path = dir + "ccc.xls";
            Dictionary<string, List<string>> st = FileReader.read(path);
            string actual = st[path][0];
            string expected = "matric";
            Assert.AreEqual(expected, actual, "Error!");
        }

        [TestMethod]
        public void xlsTest2()
        {
            string path = dir + "ccc.xls";
            Dictionary<string, List<string>> st = FileReader.read(path);
            string actual = st[path][st[path].Count - 1];
            string expected = "in";
            Assert.AreEqual(expected, actual, "Error!");
        }

        [TestMethod]
        public void xlsxTest1()
        {
            string path = dir + "Book1.xlsx";
            Dictionary<string, List<string>> st = FileReader.read(path);
            string actual = st[path][0];
            string expected = "matric";
            Assert.AreEqual(expected, actual, "Error!");
        }

        [TestMethod]
        public void xlsxTest2()
        {
            string path = dir + "Book1.xlsx";
            Dictionary<string, List<string>> st = FileReader.read(path);
            string actual = st[path][st[path].Count - 1];
            string expected = "354535";
            Assert.AreEqual(expected, actual, "Error!");
        }

        [TestMethod]
        public void txtTest1()
        {
            string path = dir + "ttt.txt";
            Dictionary<string, List<string>> st = FileReader.read(path);
            string actual = st[path][0];
            string expected = "my";
            Assert.AreEqual(expected, actual, "Error!");
        }

        [TestMethod]
        public void txtTest2()
        {
            string path = dir + "ttt.txt";
            Dictionary<string, List<string>> st = FileReader.read(path);
            string actual = st[path][st[path].Count - 1];
            string expected = "name";
            Assert.AreEqual(expected, actual, "Error!");
        }

        [TestMethod]
        public void docxTest1()
        {
            string path = dir + "322.docx";
            Dictionary<string, List<string>> st = FileReader.read(path);
            string actual = st[path][0];
            string expected = "overview";
            Assert.AreEqual(expected, actual, "Error!");
        }

        [TestMethod]
        public void docxTest2()
        {
            string path = dir + "322.docx";
            Dictionary<string, List<string>> st = FileReader.read(path);
            string actual = st[path][st[path].Count - 1];
            string expected = "understand";
            Assert.AreEqual(expected, actual, "Error!");
        }

        [TestMethod]
        public void docTest1()
        {
            string path = dir + "ddd.doc";
            Dictionary<string, List<string>> st = FileReader.read(path);
            string actual = st[path][0];
            string expected = "if";
            Assert.AreEqual(expected, actual, "Error!");
        }

        [TestMethod]
        public void docTest2()
        {
            string path = dir + "ddd.doc";
            Dictionary<string, List<string>> st = FileReader.read(path);
            string actual = st[path][st[path].Count - 1];
            string expected = "creation";
            Assert.AreEqual(expected, actual, "Error!");
        }

        [TestMethod]
        public void pdfTest1()
        {
            string path = dir + "co.pdf";
            Dictionary<string, List<string>> st = FileReader.read(path);
            string actual = st[path][0];
            string expected = "csc";
            Assert.AreEqual(expected, actual, "Error!");
        }

        [TestMethod]
        public void pdfTest2()
        {
            string path = dir + "co.pdf";
            Dictionary<string, List<string>> st = FileReader.read(path);
            string actual = st[path][st[path].Count - 1];
            string expected = "algorithm";
            Assert.AreEqual(expected, actual, "Error!");
        }

        [TestMethod]
        public void htmlTest1()
        {
            string path = dir + "test.html";
            Dictionary<string, List<string>> st = FileReader.read(path);
            string actual = st[path][0];
            string expected = "sample";
            Assert.AreEqual(expected, actual, "Error!");
        }

        [TestMethod]
        public void htmlTest2()
        {
            string path = dir + "test.html";
            Dictionary<string, List<string>> st = FileReader.read(path);
            string actual = st[path][st[path].Count - 1];
            string expected = "servlet";
            Assert.AreEqual(expected, actual, "Error!");
        }
    }
}

