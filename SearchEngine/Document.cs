﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchEngine
{
    /// <summary>
    /// Represents a Document Instance...
    /// For each occurence of the associated term a Document instance is created
    /// </summary>
    public class Document
    {
        private readonly string docID;                              //the document name
        private readonly int termPosition;                  //the position of the term in the document

        public String DocID
        {
            get { return docID; }
        }

        /// <summary>
        /// Gets the term's position in this Document 
        /// </summary>
        public int TermPositionInDocument
        {
            get { return termPosition; }
        }

        public Document(string mDocId, int mTermPosition)
        {
            docID = mDocId;
            termPosition = mTermPosition;
        }
    }
}
