﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchEngine
{
    public class Searcher
    {
        private string directory = @"c:\";
        private static DbManager db;
        private static List<string> directoryDocuments;
        private static SortedDictionary<String, Term> index;

        public static DbManager DB
        {
            get { return db; }
        }

        public List<string> SearchedTerms
        {
            get { return db.GetSearchedTerms(); }
        }

        public static SortedDictionary<String, Term> Index
        {
            get { return index; }
        }

        public static List<string> DocumentsInDirectory
        {
            get { return directoryDocuments; }
        }

        public Searcher(string directory)
        {
            this.directory = directory;
            initializeDirectory();
            loadIndexTable();
        }

        public void initializeDirectory()
        {
            
            directoryDocuments = new List<string>();
            db = new DbManager();
            List<string> storedDocuments = db.GetDocumentPaths();

            List<Dictionary<string, List<string>>> docsToBeStored = new List<Dictionary<string, List<string>>>();
            FileReader r = new FileReader();
            var files = Directory.GetFiles(directory, "*.*").Where(s => FileReader.extensions.Any(e => s.ToLower().EndsWith(e)));
            foreach (var file in files)
            {
                if (!storedDocuments.Contains(file))
                {
                    docsToBeStored.Add(FileReader.read(file));
                }
                directoryDocuments.Add(file);

            }
            writeToDatabase(docsToBeStored);
        }

        public void refreshDirectory()
        {
            initializeDirectory();
            foreach (string file in directoryDocuments)
            {
                Dictionary<string, List<string>> dict = FileReader.read(file);
                foreach (KeyValuePair<string, List<string>> k in dict)
                {
                    db.ExecuteDocumentUpdate(k.Key, k.Value);
                }
            }
            loadIndexTable();
        }

        private void writeToDatabase(List<Dictionary<string, List<string>>> docsToBeStored)
        {
            foreach (Dictionary<string, List<string>> dict in docsToBeStored)
            {
                foreach (KeyValuePair<string, List<string>> k in dict)
                {
                    db.ExecuteDocumentInsert(k.Key, k.Value);
                }
            }
        }

        private void loadIndexTable()
        {
            index = Search.GetIndexTable(db.ExecuteDocumentRead(directoryDocuments));
                       
        }

        public List<string> SearchFor(string query)
        {
            db.StoreSearchTerm(query);
            return Search.Query(index, query);
        }
    }
}
