﻿namespace SearchEngine
{
    partial class SearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainPanel = new System.Windows.Forms.Panel();
            this.resultsPanel = new System.Windows.Forms.Panel();
            this.backBTN = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.searchBTN = new System.Windows.Forms.Button();
            this.searchTB = new System.Windows.Forms.TextBox();
            this.dirLBL = new System.Windows.Forms.Label();
            this.refreshLNK = new System.Windows.Forms.LinkLabel();
            this.mainPanel.SuspendLayout();
            this.resultsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.resultsPanel);
            this.mainPanel.Controls.Add(this.label1);
            this.mainPanel.Controls.Add(this.searchBTN);
            this.mainPanel.Controls.Add(this.searchTB);
            this.mainPanel.Controls.Add(this.dirLBL);
            this.mainPanel.Controls.Add(this.refreshLNK);
            this.mainPanel.Location = new System.Drawing.Point(13, 12);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(774, 441);
            this.mainPanel.TabIndex = 0;
            // 
            // resultsPanel
            // 
            this.resultsPanel.Controls.Add(this.backBTN);
            this.resultsPanel.Controls.Add(this.listBox1);
            this.resultsPanel.Controls.Add(this.label3);
            this.resultsPanel.Controls.Add(this.label2);
            this.resultsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultsPanel.Location = new System.Drawing.Point(0, 0);
            this.resultsPanel.Name = "resultsPanel";
            this.resultsPanel.Size = new System.Drawing.Size(774, 441);
            this.resultsPanel.TabIndex = 3;
            this.resultsPanel.VisibleChanged += new System.EventHandler(this.resultsPanel_VisibleChanged);
            // 
            // backBTN
            // 
            this.backBTN.Font = new System.Drawing.Font("Hobo Std", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backBTN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.backBTN.Location = new System.Drawing.Point(705, 13);
            this.backBTN.Name = "backBTN";
            this.backBTN.Size = new System.Drawing.Size(66, 37);
            this.backBTN.TabIndex = 3;
            this.backBTN.Text = "<--Back";
            this.backBTN.UseVisualStyleBackColor = true;
            this.backBTN.Click += new System.EventHandler(this.backBTN_Click);
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.ForeColor = System.Drawing.Color.Blue;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(3, 81);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(768, 324);
            this.listBox1.TabIndex = 2;
            this.listBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseDoubleClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Rockwell", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 14);
            this.label3.TabIndex = 1;
            this.label3.Text = "No Results Found";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Hobo Std", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 27);
            this.label2.TabIndex = 0;
            this.label2.Text = "Search Results:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Baskerville Old Face", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(49, 145);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Enter Search:";
            // 
            // searchBTN
            // 
            this.searchBTN.BackColor = System.Drawing.Color.DarkGray;
            this.searchBTN.Font = new System.Drawing.Font("Hobo Std", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchBTN.ForeColor = System.Drawing.Color.Green;
            this.searchBTN.Location = new System.Drawing.Point(286, 238);
            this.searchBTN.Name = "searchBTN";
            this.searchBTN.Size = new System.Drawing.Size(176, 37);
            this.searchBTN.TabIndex = 1;
            this.searchBTN.Text = "SEARCH";
            this.searchBTN.UseVisualStyleBackColor = false;
            this.searchBTN.Click += new System.EventHandler(this.searchBTN_Click);
            // 
            // searchTB
            // 
            this.searchTB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.searchTB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.searchTB.Font = new System.Drawing.Font("Baskerville Old Face", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchTB.Location = new System.Drawing.Point(52, 171);
            this.searchTB.Name = "searchTB";
            this.searchTB.Size = new System.Drawing.Size(670, 31);
            this.searchTB.TabIndex = 0;
            this.searchTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchTB_KeyDown);
            // 
            // dirLBL
            // 
            this.dirLBL.AutoSize = true;
            this.dirLBL.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dirLBL.Location = new System.Drawing.Point(262, 216);
            this.dirLBL.Name = "dirLBL";
            this.dirLBL.Size = new System.Drawing.Size(0, 13);
            this.dirLBL.TabIndex = 4;
            // 
            // refreshLNK
            // 
            this.refreshLNK.AutoSize = true;
            this.refreshLNK.Location = new System.Drawing.Point(51, 216);
            this.refreshLNK.Name = "refreshLNK";
            this.refreshLNK.Size = new System.Drawing.Size(105, 13);
            this.refreshLNK.TabIndex = 5;
            this.refreshLNK.TabStop = true;
            this.refreshLNK.Text = "Re-initialize Directory";
            this.refreshLNK.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.refreshLNK_LinkClicked);
            // 
            // SearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 465);
            this.Controls.Add(this.mainPanel);
            this.Name = "SearchForm";
            this.Text = "SearchForm";
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            this.resultsPanel.ResumeLayout(false);
            this.resultsPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button searchBTN;
        private System.Windows.Forms.TextBox searchTB;
        private System.Windows.Forms.Panel resultsPanel;
        private System.Windows.Forms.Button backBTN;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label dirLBL;
        private System.Windows.Forms.LinkLabel refreshLNK;
    }
}