﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SearchEngine
{
    /// <summary>
    /// Contains methods that index words in documents and methods that return the highest ranking documents for a query. 
    /// </summary>
    public class Search
    {
        public static Dictionary<string, Document> docDatabase = new Dictionary<string, Document>();                     //Key: Doument Path, Value: Document Object
        private static readonly string[] stopwords = { "a", "about", "above", "across", "after", "again", "against", "all", "almost", "alone", "along", "already", "also", "although", "always", "among", "an", "and", "another", "any", "anybody", "anyone", "anything", "anywhere", "are", "area", "areas", "around", "as", "ask", "asked", "asking", "asks", "at", "away", "b", "back", "backed", "backing", "backs", "be", "became", "because", "become", "becomes", "been", "before", "began", "behind", "being", "beings", "best", "better", "between", "big", "both", "but", "by", "c", "came", "can", "cannot", "case", "cases", "certain", "certainly", "clear", "clearly", "come", "could", "d", "did", "differ", "different", "differently", "do", "does", "done", "down", "down", "downed", "downing", "downs", "during", "e", "each", "early", "either", "end", "ended", "ending", "ends", "enough", "even", "evenly", "ever", "every", "everybody", "everyone", "everything", "everywhere", "f", "face", "faces", "fact", "facts", "far", "felt", "few", "find", "finds", "first", "for", "four", "from", "full", "fully", "further", "furthered", "furthering", "furthers", "g", "gave", "general", "generally", "get", "gets", "give", "given", "gives", "go", "going", "good", "goods", "got", "great", "greater", "greatest", "group", "grouped", "grouping", "groups", "h", "had", "has", "have", "having", "he", "her", "here", "herself", "high", "high", "high", "higher", "highest", "him", "himself", "his", "how", "however", "i", "if", "important", "in", "interest", "interested", "interesting", "interests", "into", "is", "it", "its", "itself", "j", "just", "k", "keep", "keeps", "kind", "knew", "know", "known", "knows", "l", "large", "largely", "last", "later", "latest", "least", "less", "let", "lets", "like", "likely", "long", "longer", "longest", "m", "made", "make", "making", "man", "many", "may", "me", "member", "members", "men", "might", "more", "most", "mostly", "mr", "mrs", "much", "must", "my", "myself", "n", "necessary", "need", "needed", "needing", "needs", "never", "new", "new", "newer", "newest", "next", "no", "nobody", "non", "noone", "not", "nothing", "now", "nowhere", "number", "numbers", "o", "of", "off", "often", "old", "older", "oldest", "on", "once", "one", "only", "open", "opened", "opening", "opens", "or", "order", "ordered", "ordering", "orders", "other", "others", "our", "out", "over", "p", "part", "parted", "parting", "parts", "per", "perhaps", "place", "places", "point", "pointed", "pointing", "points", "possible", "present", "presented", "presenting", "presents", "problem", "problems", "put", "puts", "q", "quite", "r", "rather", "really", "right", "right", "room", "rooms", "s", "said", "same", "saw", "say", "says", "second", "seconds", "see", "seem", "seemed", "seeming", "seems", "sees", "several", "shall", "she", "should", "show", "showed", "showing", "shows", "side", "sides", "since", "small", "smaller", "smallest", "so", "some", "somebody", "someone", "something", "somewhere", "state", "states", "still", "still", "such", "sure", "t", "take", "taken", "than", "that", "the", "their", "them", "then", "there", "therefore", "these", "they", "thing", "things", "think", "thinks", "this", "those", "though", "thought", "thoughts", "three", "through", "thus", "to", "today", "together", "too", "took", "toward", "turn", "turned", "turning", "turns", "two", "u", "under", "until", "up", "upon", "us", "use", "used", "uses", "v", "very", "w", "want", "wanted", "wanting", "wants", "was", "way", "ways", "we", "well", "wells", "went", "were", "what", "when", "where", "whether", "which", "while", "who", "whole", "whose", "why", "will", "with", "within", "without", "work", "worked", "working", "works", "would", "x", "y", "year", "years", "yet", "you", "young", "younger", "youngest", "your", "yours", "z" };

        /// <summary>
        /// Takes a Query string and returns a list of files that have the keywords
        /// of the search in it, sorted by their relevance to the query in inverse order
        /// The ranking method used is tf-idf 
        /// </summary>
        /// <param name="anIndexTable">the index table</param>
        /// <param name="queryString">the query string</param>
        /// <returns>A list of files that have the keywords of the search in it, sorted by their relevance to the query in inverse order</returns>
        public static List<string> Query(SortedDictionary<String, Term> anIndexTable, string queryString)
        {
            char[] c = { ' ', ',', '.', '-', ';', ':', '!' };
            List<string> tokensFromQuery = queryString.ToLower().Split(c).ToList();
            List<Document> relevantDocs = new List<Document>();
            Dictionary<string, float> termRelevance = new Dictionary<string, float>();
            SortedDictionary<string, float> invertedIndex = new SortedDictionary<string, float>();

            //get word relevance in each document  
            foreach(string wordInQuery in tokensFromQuery)
            {
                if (anIndexTable.ContainsKey(wordInQuery))
                {
                    float relevance = 0;
                    foreach(Document d in anIndexTable[wordInQuery].Documents)
                    {
                        if (!relevantDocs.Contains(anIndexTable[wordInQuery].Documents.Find(n => n.DocID == Search.docDatabase[d.DocID].DocID)))
                        {
                            relevance += GetTermRelavance(wordInQuery, d, anIndexTable);
                            relevantDocs.Add(d);
                        }
                    }
                    termRelevance[wordInQuery] = relevance;
                }
            }

            //get documents ranking
            foreach(Document d in relevantDocs)
            {
                invertedIndex[d.DocID] = GetDocumentRanking(d, tokensFromQuery, anIndexTable, termRelevance);
            }

            List<KeyValuePair<string, float>> myList = invertedIndex.ToList();
            myList.Sort((firstPair, nextPair) => { return nextPair.Value.CompareTo(firstPair.Value); });

            foreach(KeyValuePair<string, float> x in myList)
            {
                Console.WriteLine(x.Key);
            }

            List<string> topDocuments = myList.ToDictionary(key => { return key.Key.ToLower(); }).Keys.ToList();

            return topDocuments;
        }

        /// <summary>
        /// Gets the rank value for a document on a query
        /// </summary>
        /// <param name="d">the current document</param>
        /// <param name="terms">the query in a tokenized form</param>
        /// <param name="anIndexTable">the index table</param>
        /// <param name="termRelevance">A dictionary which holds all the terms and their relevance factors</param>
        /// <returns>the rank value for a document on a query</returns>
        private static float GetDocumentRanking(Document d, List<string> terms, SortedDictionary<String, Term> anIndexTable, Dictionary<string, float> termRelevance)
        {
            float docRelevance = 0;
            foreach(string s in terms)
            {
                try
                {
                    if (anIndexTable[s].Documents.Contains(anIndexTable[s].Documents.Find(n => n.DocID == d.DocID)))
                    {
                        docRelevance += termRelevance[s];
                    }
                }
                catch (KeyNotFoundException) { }
            }
            return docRelevance;
        }

        /// <summary>
        /// Checks how relevant a word is to a document
        /// </summary>
        /// <param name="term">the term</param>
        /// <param name="d">the </param>
        /// <param name="anIndexTable">the index table</param>
        /// <returns>the relevance factor for a term/token in a document</returns>
        private static float GetTermRelavance(string term, Document d, SortedDictionary<String, Term> anIndexTable)
        {
            float tf = anIndexTable[term].GetTermCountInDocument(d.DocID);
            float idf = (float)Math.Log10(Searcher.DocumentsInDirectory.Count / anIndexTable[term].GetNoOfDistinctDocuments());
            //Console.WriteLine("term={0} tf={1} idf={2} tf-idf={3}", term, tf, idf, tf*idf);
            return tf * idf;
        }

        /// <summary>
        /// Generates the index table for a set of documents 
        /// </summary>
        /// <param name="currentSearch">
        ///     A dictionary in which the Key contains the path to the file,
        ///     and the Value contains a list of all the words in the file and all the files are relevant
        ///     to the current search query
        /// </param>
        /// 
        /// <effects>
        ///     index: A SortedDictionary in which the Key is the Word and the Value is an
        ///            object that contains a list of files where that word can be found
        /// 
        ///     docDatabase: A Dictionary in which the file path is mapped to the Document object, It populates this Dictionary
        /// </effects>
        /// 
        /// <returns>
        ///     A SortedDictionary in which the Key is the Word and the Value is an
        ///     object that contains a list of all the files where that word can be found
        /// </returns>
        public static SortedDictionary<String, Term> GetIndexTable(Dictionary<String, List<String>> currentSearch)
        {
            SortedDictionary<String, Term> index = new SortedDictionary<string,Term>();
            Dictionary<string, Document> docDatabase = new Dictionary<string, Document>();

            List<string> StopWordsList = stopwords.ToList();

            foreach (KeyValuePair<string, List<String>> x in currentSearch)
            {
                for (int i = 0; i < currentSearch[x.Key].Count; i++)
                {
                    if (!StopWordsList.Contains(currentSearch[x.Key][i]))
                    {
                        //new one
                        if (!index.ContainsKey(currentSearch[x.Key][i]))
                        {
                            //Console.WriteLine(x.Key);
                            Term t = new Term(currentSearch[x.Key][i]);
                            Document d = new Document(x.Key, i);
                            if (!docDatabase.ContainsKey(d.DocID))
                            {
                                docDatabase.Add(d.DocID, d);
                            }
                            t.AddRelatedDocument(d);
                            index.Add(currentSearch[x.Key][i], t);
                        }
                        //existing one
                        else
                        {
                            Document d = new Document(x.Key, i);
                            if (!docDatabase.ContainsKey(d.DocID))
                            {
                                docDatabase.Add(d.DocID, d);
                            }
                            index[currentSearch[x.Key][i]].AddRelatedDocument(d);
                        }
                    }
                }
            }

            index.OrderBy(key => key.Key);
            Search.docDatabase = docDatabase;
            return index;
        }
    }
}