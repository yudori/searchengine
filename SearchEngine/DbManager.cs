﻿using Finisar.SQLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace SearchEngine
{
    public class DbManager
    {
        SQLiteConnection connection;
        SQLiteCommand command;
        SQLiteDataReader reader;

        private void Connect()
        {
            connection = new SQLiteConnection("Data Source=SearchDatabase.db;Version=3;Compress=True;");
            connection.Open();
        }

        private int Execute(string query)
        {
            try
            {
                Connect();
                command = connection.CreateCommand();
                command.CommandText = query;
                command.ExecuteNonQuery();
                connection.Close();

            }
            catch (Exception) { connection.Close(); return 0; }
            return 1;
        }

        public int ExecuteDocumentInsert(string docPath, List<string> tokens)
        {
            try
            {
                Connect();
                string query = @"INSERT INTO Document (doc_path, tokens) VALUES ('" + docPath + "', @tokenlist)";

                command = connection.CreateCommand();
                command.CommandText = query;
                command.Parameters.Add("@tokenlist", DbType.Binary).Value = Pack(tokens);
                command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception) { connection.Close(); return 0; }
            return 1;
        }

        public Dictionary<String, List<String>> ExecuteDocumentRead(List<string> doc_paths)
        {
            Dictionary<String, List<String>> result = new Dictionary<String, List<String>>();
            try
            {
                Connect();

                foreach (string path in doc_paths)
                {
                    string query = "SELECT doc_path, tokens FROM Document WHERE (doc_path = '" + path + "')";
                    command = connection.CreateCommand();
                    command.CommandText = query;
                    reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        //String path = reader.GetString(0);
                        byte[] buffer = GetBytes(reader, 1);
                        List<String> tokens = Unpack(buffer);
                        result.Add(reader.GetString(0), tokens);
                    }
                }                
            }
            catch (Exception){}
            finally{ connection.Close(); }
            return result;
        }
        public int ExecuteDocumentUpdate(string docPath, List<string> tokens)
        {
            try
            {
                Connect();
                string query = @"UPDATE Document SET tokens = @tokenlist WHERE doc_path = '" + docPath + "';";

                command = connection.CreateCommand();
                command.CommandText = query;
                command.Parameters.Add("@tokenlist", DbType.Binary).Value = Pack(tokens);
                command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception) { connection.Close(); return 0; }
            return 1;
        }

        public List<string> GetDocumentPaths()
        {
            List<string> paths = new List<string>();
            try
            {
                Connect();
                command = connection.CreateCommand();
                command.CommandText = "SELECT doc_path FROM Document";

                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    paths.Add(reader.GetString(0));
                }
            }
            catch (Exception) { }
            finally { connection.Close(); }
            return paths;
        }

        public int StoreSearchTerm(string term)
        {
            try
            {
                Connect();
                string query = @"INSERT INTO Searched_Terms (Term) VALUES ('" + term + "')";

                command = connection.CreateCommand();
                command.CommandText = query;
                command.ExecuteNonQuery();
            }
            catch (Exception) { connection.Close(); return 0; }
            return 1;
        }

        public List<string> GetSearchedTerms()
        {
            List<string> terms = new List<string>();
            try
            {
                Connect();
                string query = @"SELECT Term FROM Searched_Terms";
                command = connection.CreateCommand();
                command.CommandText = query;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (!terms.Contains(reader.GetString(0)))
                    {
                        terms.Add(reader.GetString(0));
                    }
                }
                connection.Close();
            }
            catch (Exception) { }
            finally { connection.Close(); }
            return terms;
        }

        private byte[] Pack(List<string> list)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, list);
                return ms.GetBuffer();
            }
        }

        private List<string> Unpack(byte[] items)
        {
            BinaryFormatter bf = new BinaryFormatter();
            List<string> list = new List<string>();

            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(items, 0, items.Length);
                ms.Position = 0;
                list = bf.Deserialize(ms) as List<string>;
            }
            return list;
        }
                
        private byte[] GetBytes(SQLiteDataReader reader, int columnIndex)
        {
            const int CHUNK_SIZE = 2 * 1024;
            byte[] buffer = new byte[CHUNK_SIZE];
            long bytesRead;
            long fieldOffset = 0;
            using (MemoryStream stream = new MemoryStream())
            {
                while ((bytesRead = reader.GetBytes(columnIndex, fieldOffset, buffer, 0, buffer.Length)) > 0)
                {
                    stream.Write(buffer, 0, (int)bytesRead);
                    fieldOffset += bytesRead;
                }
                return stream.ToArray();
            }
        }


        
    }
}
