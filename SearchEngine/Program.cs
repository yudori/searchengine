﻿using System;
using System.Windows.Forms;

namespace SearchEngine
{
    class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.Run(new SearchForm(@"C:\SearchEngine\test_files\"));
        }
    }
}
