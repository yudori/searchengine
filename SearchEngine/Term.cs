﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchEngine
{
    /// <summary>
    /// Represents a single Term/Word
    /// </summary>
    public class Term
    {
        private readonly string termName;                                        //the term name
        private List<Document> listOfRelatedDocuments = new List<Document>();    //the documents where the term can be found in along with the position in each document

        public Term(String mTermName)
        {
            termName = mTermName;
        }

        /// <summary>
        /// Gets the unique identifier for this term
        /// </summary>
        public String TermId
        {
            get { return termName; }
        }

        /// <summary>
        /// returns the files that contain this term 
        /// </summary>
        public List<Document> Documents
        {
            get { return listOfRelatedDocuments; }
        }

        /// <summary>
        /// Adds a new file to this term 
        /// </summary>
        /// <param name="d">the document</param>
        public void AddRelatedDocument(Document d)
        {
            listOfRelatedDocuments.Add(d);
        }

        /// <summary>
        /// Adds a related document to the term
        /// </summary>
        /// <param name="mDocName">the document name</param>
        /// <param name="mTermPositionInDocument">the position of the term in the document</param>
        public void AddRelatedDocument(String mDocId, int mTermPosition)
        {
            Document d = new Document(mDocId, mTermPosition);
            listOfRelatedDocuments.Add(d);
        }

        /// <summary>
        /// Gets the number of files in which this term appears in
        /// </summary>
        /// <returns>the number of files in which this term appears in</returns>
        public int GetNoOfDistinctDocuments()
        {
            List<Document> temp = new List<Document>();
            foreach (Document d in Documents)
            {
                if (!temp.Contains(Documents.Find(n => n.DocID == d.DocID))) { temp.Add(d); }
            }
            return temp.Count;
        }

        /// <summary>
        /// Gets the number of times a Term appears in a Document
        /// </summary>
        /// <param name="mDocId"></param>
        /// <returns>the number of times this term appears in a given file. returns 0 if the file is none existent</returns>
        public int GetTermCountInDocument(String mDocId)
        {
            return Documents.FindAll(n => n.DocID == mDocId).Count;
        }
    }
}
