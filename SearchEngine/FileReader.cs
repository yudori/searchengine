﻿using Code7248.word_reader;
using Excel;
using HtmlAgilityPack;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

namespace SearchEngine
{
    /// <summary>
    /// 
    /// </summary>
    public class FileReader
    {
        public static readonly List<string> extensions = new List<string> { ".pdf", ".doc", ".docx", ".ppt", ".pptx", ".xls", ".xlsx", ".txt", ".html", ".xml" };
        /// <summary>
        /// Reads each supported file in the directory and returns a list of tokens for each file
        /// </summary>
        /// <param name="directory">Directory from which all files are to be read</param>
        /// <returns></returns>
        public static List<Dictionary<string, List<string>>> readAll(string directory)
        {
            List<Dictionary<string, List<string>>> results = new List<Dictionary<string, List<string>>>();

            var files = Directory.GetFiles(directory, "*.*").Where(s => extensions.Any(e => s.ToLower().EndsWith(e)));
            foreach (var file in files)
            {
                results.Add(read(file));
            }
            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Dictionary<string, List<string>> read(string path)
        {
            try
            {
                string extension = Path.GetExtension(path).ToLower();
                Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();
                switch (extension)
                {
                    case ".xml":
                        result = readXmlFile(path);
                        break;

                    case ".xls":
                    case ".xlsx":

                        result = readExcelFile(path, extension);
                        break;
                    case ".doc":
                    case ".docx":
                        result = readWordFile(path);
                        break;

                    case ".txt":
                        result = readTextFile(path);
                        break;

                    case ".pdf":
                        result = readPdfFile(path);
                        break;
                    case ".html":
                        result = readHtmlFile(path);
                        break;

                    case ".ppt":
                    case ".pptx":
                        result = readPptFile(path);
                        break;

                    default:
                        return null;
                }
                return result;
            }
            catch (DirectoryNotFoundException e) { MessageBox.Show(e.Message); return null; }
            catch (IOException e) { MessageBox.Show(e.Message); return null; }
            catch (NullReferenceException e) { MessageBox.Show(e.Message); return null; }
            catch (Exception e) { MessageBox.Show(e.Message); return null; }
            
        }

        /// <summary>
        /// Reads and tokenizes powerpoint file types
        /// </summary>
        /// <param name="path">The full path to the file including file name</param>
        /// <returns></returns>
        private static Dictionary<string, List<string>> readPptFile(string path)
        {
            //code missing
            return null;
        }

        /// <summary>
        /// Reads and tokenizes HTML file types
        /// </summary>
        /// <param name="path">The full path to the file including file name</param>
        /// <returns></returns>
        private static Dictionary<string, List<string>> readHtmlFile(string path)
        {
            List<string> result = new List<string>();
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.Load(path);

            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//text()"))
            {
                result.AddRange(alphaNumericValueOf(node.InnerText).Trim().ToLower().Split(new string[] { "\r\n", " " }, StringSplitOptions.RemoveEmptyEntries));
            }

            Dictionary<string, List<string>> res = new Dictionary<string, List<string>>();
            res.Add(path, result);
            return res;
        }

        /// <summary>
        /// Reads and tokenizes PDF file types
        /// </summary>
        /// <param name="path">The full path to the file including file name</param>
        /// <returns></returns>
        private static Dictionary<string, List<string>> readPdfFile(string path)
        {
            List<string> result = new List<string>();
            PdfReader reader = new PdfReader(path);
            for (int page = 1; page <= reader.NumberOfPages; page++)
            {
                ITextExtractionStrategy its = new LocationTextExtractionStrategy();
                String s = PdfTextExtractor.GetTextFromPage(reader, page, its);
                s = Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(s)));
                result.AddRange(alphaNumericValueOf(s.Trim().ToString()).ToLower().Split(new string[] { "\r\n", " " }, StringSplitOptions.RemoveEmptyEntries));
            }

            Dictionary<string, List<string>> res = new Dictionary<string, List<string>>();
            res.Add(path, result);
            return res;
        }

        /// <summary>
        /// Reads and tokenizes doc and docx file types
        /// </summary>
        /// <param name="path">The full path to the file including file name</param>
        /// <returns></returns>
        private static Dictionary<string, List<string>> readWordFile(string path)
        {
            List<string> result = new List<string>();
            TextExtractor extractor = new TextExtractor(path);
            string temp = extractor.ExtractText().Trim();

            result.AddRange(alphaNumericValueOf(temp).ToLower().Split(new string[] { "\r\n", " " }, StringSplitOptions.RemoveEmptyEntries));

            Dictionary<string, List<string>> res = new Dictionary<string, List<string>>();
            res.Add(path, result);
            return res;
        }

        /// <summary>
        /// Reads and tokenizes XML file types
        /// </summary>
        /// <param name="path">The full path to the file including file name</param>
        /// <returns></returns>
        private static Dictionary<string, List<String>> readXmlFile(string path)
        {
            List<string> result = new List<string>();
            XmlTextReader reader = new XmlTextReader(path);
            XmlNodeType type;
            while (reader.Read())
            {
                type = reader.NodeType;
                if (!type.Equals(XmlNodeType.Element))
                {
                    result.AddRange(alphaNumericValueOf(reader.Value.ToString()).ToLower().Split(new string[] { "\r\n", " " }, StringSplitOptions.RemoveEmptyEntries));
                }
            }

            Dictionary<string, List<string>> res = new Dictionary<string, List<string>>();
            res.Add(path, result);
            return res;
        }

        /// <summary>
        /// Reads and tokenizes Text file types
        /// </summary>
        /// <param name="path">The full path to the file including file name</param>
        /// <returns></returns>
        private static Dictionary<string, List<String>> readTextFile(string path) 
        {
            List<string> result = new List<string>();
            StreamReader sReader = new StreamReader(path);
            string temp = sReader.ReadToEnd().Trim();
            result.AddRange(alphaNumericValueOf(temp).ToLower().Split(new string[] { "\r\n", " " }, StringSplitOptions.RemoveEmptyEntries));

            Dictionary<string, List<string>> res = new Dictionary<string, List<string>>();
            res.Add(path, result);
            return res;
        }

        /// <summary>
        /// Reads and tokenizes .xls and .xlsx file types
        /// </summary>
        /// <param name="path">The full path to file including the file name</param>
        /// <param name="extension"> The extension of the excel document</param>
        /// <returns></returns>
        private static Dictionary<string, List<string>> readExcelFile(string path, string extension) 
        {
            List<string> result = new List<string>();
            FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader;

            if (extension.Equals(".xls"))
                excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
            else
                excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            excelReader.IsFirstRowAsColumnNames = false;
            DataSet workbook = excelReader.AsDataSet();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < workbook.Tables.Count; i++)
            {
                DataTable worksheet = workbook.Tables[i];
                    
                IEnumerable<DataRow> dt = from DataRow row in worksheet.Rows select row;
                foreach (DataRow d in dt)
                {
                    if (AllRowsEmpty(d))
                    {
                        break;
                    }
                    for (int j = 0; j < worksheet.Columns.Count; j++)
                    {
                        string[] arr = d[j].ToString().Split();
                        foreach (string a in arr)
                        {
                            sb.Append(a + " ");
                        }
                    }
                }
            }
            result.AddRange(alphaNumericValueOf(sb.ToString()).ToLower().Split(new string[] { "\r\n", " " }, StringSplitOptions.RemoveEmptyEntries));

            Dictionary<string, List<string>> res = new Dictionary<string, List<string>>();
            res.Add(path, result);
            return res;
        }

        /// <summary>
        /// Checks if all the columns of a DataRow are empty
        /// </summary>
        /// <param name="d"></param>
        /// <returns>True if all rows are empty, false otherwise</returns>
        private static bool AllRowsEmpty(DataRow d)
        {

            foreach (DataColumn column in d.Table.Columns)
                if (!d.IsNull(column))
                    return false;

            return true;
        }
     
        /// <summary>
        /// Formats String to remove all non-alphanumeric symbols
        /// </summary>
        /// <param name="inputString">the string to be formatted</param>
        /// <returns>alphanumeric string value of input string</returns>
        private static string alphaNumericValueOf(string inputString)
        {
            Regex regex = new Regex("[^a-zA-Z0-9]");
            return regex.Replace(inputString, " ");
        }
    }
}