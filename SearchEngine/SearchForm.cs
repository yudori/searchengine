﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

namespace SearchEngine
{
    public partial class SearchForm : Form
    {
        bool canSearch = false;
        private string directory = "";
        Searcher searcher;
        
        public SearchForm(string directory)
        {
            this.directory = directory;
            
            InitializeComponent();
            mainPanel.Visible = true;
            resultsPanel.Visible = true;
            resultsPanel.Visible = false;
        }

        private void searchBTN_Click(object sender, EventArgs e)
        {
            performSearch();
        }

        private void backBTN_Click(object sender, EventArgs e)
        {
            resultsPanel.Visible = false;
            searchTB.Text = "";
        }

        private void searchTB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                performSearch();
            }
        }

        private void performSearch()
        {
            listBox1.Items.Clear();
            label3.Text = "No Results found";
            if (canSearch)
            {
                string term = searchTB.Text;

                //var watch = Stopwatch.StartNew();
                List<string> QueryResults = searcher.SearchFor(term);
                //watch.Stop();
                //MessageBox.Show("Time Taken: "+watch.ElapsedMilliseconds);

                if (QueryResults.Count > 0)
                {
                    label3.Text = QueryResults.Count + " Result(s) found for '" + term + "'";
                    foreach (String result in QueryResults)
                    {
                        listBox1.Items.Add(result);
                    }
                }
                else
                {
                    label3.Text = "No Results found for '" + term + "'";
                }
                resultsPanel.Visible = true;
            }
        }

        private void resultsPanel_VisibleChanged(object sender, EventArgs e)
        {
            if (((Panel)sender).Visible == false)
            {
                resultsPanel.Visible = false;
                canSearch = false;
                dirLBL.Text = "Initializing Search Directory...";
                dirLBL.Refresh();
                searcher = new Searcher(directory);
                List<string> searched = searcher.SearchedTerms;
                AutoCompleteStringCollection ac = new AutoCompleteStringCollection();
                ac.AddRange(searched.ToArray());
                searchTB.AutoCompleteCustomSource = ac;

                dirLBL.Text = "Search Directory: " + directory;
                canSearch = true;
            }
        }

        private void refreshLNK_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            dirLBL.Text = "Initializing Search Directory...";
            dirLBL.Refresh();
            canSearch = false;
            searcher.refreshDirectory();
            canSearch = true;
            dirLBL.Text = "Search Directory: " + directory;
            
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                int index = listBox1.IndexFromPoint(e.Location);
                if (listBox1.SelectedItem != null)
                {
                    Process.Start(listBox1.Items[index].ToString());
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
